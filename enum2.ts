enum StatusCodes {
    NotFound = 404,
    Success = 200 ,
    Accept = 202,
    BadRequest = 400
}

console.log(StatusCodes.NotFound)